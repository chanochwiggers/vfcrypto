import {connect} from 'react-redux'
import { ButtonToolbar, DropdownButton, MenuItem }  from 'react-bootstrap'
import { setTargetCurrency } from './currencies/fetchCurrencies';

const targetCurrencies = [
'USD',
'AUD',
'BRL',
'CAD',
'CHF',
'CLP',
'CNY',
'CZK',
'DKK',
'EUR',
'GBP',
'HKD',
'HUF',
'IDR',
'ILS',
'INR',
'JPY',
'KRW',
'MXN',
'MYR',
'NOK',
'NZD',
'PHP',
'PKR',
'PLN',
'RUB',
'SEK',
'SGD',
'THB',
'TRY',
'TWD',
'ZAR',
]

function TargetCurrencySelector({targetCurrency, setCurrency}) {
  const handleSelect = code => () => {
    setCurrency(code)
  }

  return (
    <ButtonToolbar>
      <DropdownButton id={'currencySelector'} title={targetCurrency} bsSize="small" bsStyle='default'>
        {targetCurrencies.map(curr => {
          return (
            <MenuItem key={curr} onSelect={handleSelect(curr)} disabled={curr===targetCurrency}>{curr}</MenuItem>
          )
        })}
      </DropdownButton>
    </ButtonToolbar>  )
}

const mapStateToProps = ({targetCurrency}) => {
  return {
    targetCurrency
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setCurrency: code => dispatch(setTargetCurrency(code))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TargetCurrencySelector)
