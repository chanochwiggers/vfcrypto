import {connect} from 'react-redux'
import { Row, Col } from 'react-bootstrap'

function ColumnTitles() {
  return (
    <div  className="list_header">
      <style jsx>{`
        .list_header {
          font-size: 8px;
          font-weight: 800;
          padding: 4px;
          color: #abafbe;
          border-top: 1px solid #d5e1ea;
          border-bottom: 1px solid #d5e1ea;
          background-color: #ebf0f4;
        }
      `}</style>
      <Row>
        <Col md={2} mdOffset={2}>CRYPTOCURRENCY</Col>
        <Col md={2}>PRICE</Col>
        <Col md={2}>MARKET CAP</Col>
        <Col md={2}>24H CHANGE</Col>
      </Row>
    </div>
  )
}

export default connect()(ColumnTitles)
