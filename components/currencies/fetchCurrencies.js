import superagent from 'superagent'

const FETCH_CURRENCIES = 'FETCH_CURRENCIES'
const CURRENCIES_DATA = 'CURRENCIES_DATA'
export const CURRENCIES_ERROR = 'CURRENCIES_ERROR'
export const SET_TARGET_CURRENCY = 'SET_TARGET_CURRENCY'


// https://api.coinmarketcap.com/v1/ticker/?limit=10
const CURRENCY_URI = process.env.FETCH_CURRENCIES_URI || `http://localhost:3000/static/crypto_currencies.json?limit=10`

export function setTargetCurrency(targetCurrency) {
  return {
    type: SET_TARGET_CURRENCY,
    targetCurrency
  }
}

/**
 * Load currency data 
 * 
 * Loads the data from coinmarketcap 
 * It uses the user's preferred the target currency to show market cap and price 
 */
async function fetchCurrenciesData(store) {
  try {
    const targetCurrency = store.getState().targetCurrency
    const result = await superagent.get(`${CURRENCY_URI}&convert=${targetCurrency}`)
    const currencies = result.body
    store.dispatch(currenciesData(currencies, Date.now()))

  } catch (reason) {
    store.dispatch(currenciesError(reason))

  }
}

export function fetchCurrenciesAction() {
  return {
    type: FETCH_CURRENCIES
  }
}

export function fetchCurrenciesMiddleware(store) {
  return next => async action => {
    next(action)
    if(action.type===FETCH_CURRENCIES) {
      fetchCurrenciesData(store)

    } else if(action.type===SET_TARGET_CURRENCY) {
      fetchCurrenciesData(store)

    }
  }
}

export function currenciesData(currencies, lastUpdated) {
  return {
    type: CURRENCIES_DATA,
    currencies,
    lastUpdated
  }
}

export function currenciesError(message) {
  return {
    type: CURRENCIES_ERROR,
    message
  }
}

// reducer
export function currencies(state=[], action) {
  if(action.type===CURRENCIES_DATA) {
    return action.currencies
  }
  else {
    return state
  }
}

// reducer
export function currencyError(state=null, action) {
  if(action.type===CURRENCIES_ERROR) return action.message
  else return state
}

// reducer
export function lastUpdated(state=null, action) {
  if(action.type===CURRENCIES_DATA) return action.lastUpdated
  else return state
}

// reducer
export function targetCurrency(state='USD', action) {
  if(action.type===SET_TARGET_CURRENCY) {
    return action.targetCurrency
  }
  else return state
}
