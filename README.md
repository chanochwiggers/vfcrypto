# Introduction
This is a next SPA (react, redux, nextjs) implementation of a tech task for Virtual Forge.

It loads top 10 currencies and then allows the user to see more detailed information about trading and available stock.

## Store
The store contains the top 10 currencies data and the currency the user would like to see market cap and price in. This value is USD by default.

# Conventions

'''javascript
/**
 * TODO.md - this file is used to detail day-to-day tasks raised during implementation outside of asana. The NOW heading should be empty
 *           before committing to git. The NEXT header contains tasks for future rounds of dev and should be added to sprint stories or backlog
 *           @todo - this is used to show where there are queries to raise with the business/PO
 */
'''
