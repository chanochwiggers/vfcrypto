function Supply ({ name, value, symbol }) {
  const formatter = new Intl.NumberFormat('en-GB', {
    minimumFractionDigits: 0
  })

  return (
    <div className='container'>
      <style jsx>{`
        .container {
          height: 80px;
        }
        .datum {
          top: 25%;
          position: absolute;
          margin: 0;
        }
        .supply {
          line-height: 1.7em;
        }
        .datum_value {
          display: inline;
          font-size: 14px;
          color: white;
        }
        .currency_code {
          margin-left: 6px;
          display: inline;
          color: #2dd47f;
          font-size: 8px;
        }
      `}</style>
      <div className='datum'>
        <div className="supply">{ name }</div>
        <div className="supply">
          <span className="datum_value">{formatter.format(value)}</span><span className='currency_code'>{symbol}</span>
        </div>
      </div>
    </div>
  )
}

export default Supply
