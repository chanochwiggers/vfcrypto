import { createStore, applyMiddleware, combineReducers } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'

import { currencyError, currencies, lastUpdated, fetchCurrenciesMiddleware, targetCurrency } from './components/currencies/fetchCurrencies'
import { currency, currencyMiddleware } from './components/currency/fetchCurrencyDetail';

const blankInitialState = {
  currencies: [],
  currencyError: {},
  lastUpdated: null,
  targetCurrency: 'USD',
  currency: {}
}

export function initializeStore (initialState = blankInitialState) {
  return createStore(
    combineReducers({currencies, lastUpdated, currencyError, targetCurrency, currency}), 
    initialState, 
    composeWithDevTools(applyMiddleware(fetchCurrenciesMiddleware, currencyMiddleware)))
}
