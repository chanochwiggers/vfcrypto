import Amount from '../Amount'

function Price ({ name, value, currency }) {
  return (
    <div className='container'>
      <style jsx>{`
        .container {
          height: 80px;
        }
        .datum {
          top: 25%;
          position: absolute;
          margin: 0;
        }
        .datum_name {
          line-height: 1.7em;
        }
        .datum_value {
          line-height: 1.7em;
          font-size: 14px;
          color: white;
        }
        .currency_code {
          color: #2dd47f;
          font-size: 10px;
        }
      `}</style>
      <div className='datum'>
        <div className="datum_name">{ name }</div>
        <div className="datum_value"><Amount value={value} currency={currency} /></div>
      </div>
    </div>
  )
}

export default Price
