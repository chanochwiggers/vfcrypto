import superagent from 'superagent'
import { currenciesError, SET_TARGET_CURRENCY } from "../currencies/fetchCurrencies";

const CURRENCY_DETAIL = 'CURRENCY_DETAIL'
const CURRENCY_DATA = 'CURRENCY_DATA'

const CURRENCY_URI = process.env.FETCH_CURRENCY_URI || `https://api.coinmarketcap.com/v1/ticker/`

/**
 * Load the currency detail.
 */
export function currencyDetailAction(currencyId) {
  return {
    type: CURRENCY_DETAIL,
    currencyId
  }
}

function currencyDataAction(currency) {
  return {
    type: CURRENCY_DATA,
    currency
  }
}

const fetchCurrencyData = async (store, currencyId) => {
  try {
    const targetCurrency = store.getState().targetCurrency
    const currencyDataUri = `${CURRENCY_URI}${currencyId}/?convert=${targetCurrency}`
    const result = await superagent.get(currencyDataUri)
    const currs = result.body
    const currencyData = currs.find(item => item.id===currencyId)
    store.dispatch(currencyDataAction(currencyData, Date.now()))

  } catch (reason) {
    store.dispatch(currenciesError(reason))

  }
}

export function currencyMiddleware(store) {
  return next => async action => {
    next(action)
    if(action.type===CURRENCY_DETAIL) {
      fetchCurrencyData(store, action.currencyId)

    } else if(action.type===SET_TARGET_CURRENCY && window.location.pathname.indexOf('/crypto')===0) {
      fetchCurrencyData(store, store.getState().currency.id)

    }
  }
}

// reducer
export function currency(state={}, action) {
  if(action.type===CURRENCY_DATA) {
    return action.currency
  }
  else {
    return state
  }
}
