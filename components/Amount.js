export default function Amount ({ value, currency }) {
  const formatter = new Intl.NumberFormat('en-GB', {
    style: 'currency',
    currency: currency,
    minimumFractionDigits: 2
  })

  return (
    <div  className="currency_value">
      <style jsx>{`
        .currency_code {
          padding-right: 10px;
          color: #abb8d5;
        }
      `}</style>
      {formatter.formatToParts && formatter.formatToParts(value).map((part,i)=>(
        <span key={i} className={part.type==='currency'?'currency_code':''}>{part.value}</span>
      ))}
    </div>
  )
}
