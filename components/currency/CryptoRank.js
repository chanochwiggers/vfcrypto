/**
 * Show the crypto currency rank
 * 
 * Displays a circle background with the numeric rank of the crypto
 * 
 * @param {Object} props
 * @param {String} props.rank - the top 10 rank of the crypto currency 
 */
function CryptoRank ({ rank }) {
  return (
    <div>
      <style jsx>{`
        .rank_badge {
          display: inline;
          border-radius: 50%;
          padding: 16px 20px;
          margin-left: 8px;
          background-color: #1e385a;
          color: #5aa7f2;
        }

      `}</style>

      RANK <div className='rank_badge'>{rank}</div>
    </div>
  )
}

export default CryptoRank
