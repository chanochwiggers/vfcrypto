import { connect } from 'react-redux'
import { Row, Col } from 'react-bootstrap'
import Link from 'next/link'
import Updated from '../Updated';
import TargetCurrencySelector from '../TargetCurrencySelector';
import Amount from '../Amount'

/**
 * The header for the page which shows detailed cryptocurrency statistics 
 * 
 * The design of the two pages is different each has its own header
 * 
 * @param {Object} props - react props
 * @param {Date} props.lastUpdate - the date that currency data was last fetched 
 * @param {String} props.xcurrency - show cryptocurrency's values based on this target currency
 */
function Header({ currency, targetCurrency }) {
  return (
    <div className={'brand'}>
      <style jsx>{`
          .brand {
            padding: 20px;
            color: #606c84;
            font-size: 14px;
            font-weight: bold;
          }
          .code {
            font-size: 8px;
          }
          .sprite-logo {
            display: inline-block;
            margin-bottom: -3px;
            margin-left: 80%;
          }
      `}</style>
      {currency &&
        <Row>
          <Col md={1}><Link href="/"><a>&lt;-</a></Link></Col>
          <Col md={1}><div className={`s-s-${currency.id} currency-logo-sprite sprite-logo`} title={currency.id}></div></Col>
          <Col md={1}>{currency.name}<div className='code'>{currency.symbol}</div></Col>
          <Col md={1}><Amount value={currency[`price_${targetCurrency.toLowerCase()}`]} currency={targetCurrency} /></Col>
          <Col md={4}><Updated lastUpdate={currency.last_updated*1000} /></Col>
          <Col md={4}><TargetCurrencySelector /></Col>
        </Row>
      }
    </div>
  )
}

const mapStateToProps = ({targetCurrency}) => {
  return { targetCurrency}
}

export default connect(mapStateToProps)(Header)