import {connect} from 'react-redux'
import { Row, Col } from 'react-bootstrap'
import Updated from '../Updated';
import TargetCurrencySelector from '../TargetCurrencySelector';

/**
 * The header for the home page which shows the current top 10 crypto currencies
 * 
 * The design of two pages is different so each has its own header
 * 
 * @param {Object} props - react props
 * @param {Date} props.lastUpdate - the date that currencies data was last fetched 
 * @param {String} props.xcurrency - show cryptocurrency's values based on this target currency
 */
function Header({ lastUpdated, currency }) {
  return (
    <div className={'brand'}>
      <style jsx>{`
          .brand {
            padding: 20px;
            color: #606c84;
            font-size: 14px;
          }
        `}</style>
      <Row>
        <Col md={8}>VFCrypto <Updated lastUpdate={lastUpdated} /></Col>
        <Col md={4}><TargetCurrencySelector /></Col>
      </Row>
    </div>
  )
}

function mapStateToProps ({ lastUpdated }) {
  return { lastUpdated }
}

export default connect(mapStateToProps)(Header)
