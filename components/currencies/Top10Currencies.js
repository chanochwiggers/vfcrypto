import {connect} from 'react-redux'
import { Grid, Row, Col } from 'react-bootstrap'
import Header from './Header';
import ColumnTitles from './ColumnTitles';
import SiteHead from '../../SiteHead';
import Amount from '../Amount';
import Link from 'next/link'
import ChangeIndicator from './ChangeIndicator';

function Top10Currencies ({ currencies, targetCurrency }) {
  return (
    <div>
      <SiteHead title="Top 10 Currencies" />
      <Grid fluid={true}>
        <Header />
        <ColumnTitles />
        {currencies && currencies.map(currency => (
          <div  className="crypto_summary" key={currency.name}>
            <style jsx>{`
              .crypto_summary {
                font-size: 10px;
                padding-top: 10px;
                padding-bottom: 10px;
                color: #000;
                cursor: pointer;
              }
              :hover {
                font-weight: bolder; 
                border-bottom: 1px solid lightgrey; 
              }
              .sprite-logo {
                width: 16px;
                height: 16px;
                display: inline-block;
                margin-bottom: -3px;
                margin-right: 8px;
                margin-left: 8px;
              }
    
            `}</style>
            <Link href={`/crypto?currency=${currency.id}`}>
              <Row>
                <Col md={2} mdOffset={2}>{currency.rank} <div className={`s-s-${currency.id} currency-logo-sprite sprite-logo`} title={currency.id}></div>{currency.name}</Col>
                <Col md={2}><Amount value={currency[`price_${targetCurrency.toLowerCase()}`]} currency={targetCurrency} /></Col>
                <Col md={2}><Amount value={currency[`market_cap_${targetCurrency.toLowerCase()}`]} currency={targetCurrency} /></Col>
                <Col md={2}><ChangeIndicator percentChange={currency.percent_change_24h} /></Col>
              </Row>
            </Link>
          </div> 
        ))}
      </Grid>
    </div>
  )
}

const mapStateToProps = ({currencies, targetCurrency}) => {
  return { currencies, targetCurrency }
}

export default connect(mapStateToProps)(Top10Currencies)
