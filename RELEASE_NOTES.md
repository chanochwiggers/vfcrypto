# V0.2.3 Add arrows with color to change indicator

# V0.2.2 Add logos for cryptocurrencies
1. Add logos for cryptocurrencies
1. Format currencies as per design (grey/blue currency symbol)
1. Format supply to en-GB locale

# V0.2.1 Fetch additional detail for cryptocurrency
1. Added loading of cryptocurrency data
1. Add support for changing target currency to cryptocurrency detail view
1. In crypto detail view, last updated is the date from the api (not the date of fetch)
## Other
1. Formatted supply values as integer


# V0.2.0 Fetch data for Top 10 Currencies
https://api.coinmarketcap.com/v1/ticker/?limit=10&convert={target_currency}

1. Added loading of crypto currencies data from coinmarketcap.com
1. Added selection of an export currency for currency conversion - price and market cap will be shown in this target currency

## Other
1. Moved currencies middleware out of the middleware folder and into the currencies component. (There's no current place for shared functions)
1. Extracted an Amount react component to format currency data
1. Extracted a Scheduler component to load data and create a schedule to refresh the currency data
1. Deleted default next code - components/counter.js, 
1. Added a static folder to serve a cache of the currency data for local testing
1. Added TODO.md - this is a temporary store of tasks


# V0.1.0 First release, mockup of front end
Mock up of the app with static data
