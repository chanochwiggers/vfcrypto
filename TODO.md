# NOW
# NEXT
1. Add api update date to top10
1. Compile in build-time env vars for configuration
1. What to show if no data has been loaded

# Raise as queries
* @todo query whether to follow redirects
* @todo query - implement error log
* @todo query - what should the currency row :hover effect be for home page (Top10Currencies)
* @todo query - Implement as websocket instead? This could use caching and be more resilient against API outages/breaking changes
* @todo query - could persist preferred target currency in local storage
* @todo query - should the currency detail be reloaded every minute also?
* @todo query - currency symbol formatting javascript engine includes which dollar (for example US$) - is this acceptable?
* @todo query - are we offering users locale formatting? How is user indicating preferred locale?
