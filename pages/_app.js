import App, {Container} from 'next/app'
import React from 'react'
import withReduxStore from '../lib/with-redux-store'
import { Provider } from 'react-redux'
import Scheduler from '../components/Scheduler'

class MyApp extends App {
  render () {
    const {Component, pageProps, reduxStore} = this.props
    return (
      <Container>
        <Provider store={reduxStore}>
          <Scheduler>
            <Component {...pageProps} />
          </Scheduler>
        </Provider>
      </Container>
    )
  }
}

export default withReduxStore(MyApp)
