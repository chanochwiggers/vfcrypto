import { Component } from 'react'
import { connect } from 'react-redux'
import { fetchCurrenciesAction } from './currencies/fetchCurrencies';
/**
 * This is a wrapper component which makes sure there is a data scheduler defined
 * to refresh crypto currency data periodically
 */
class Scheduler extends Component {
  componentDidMount () {
    this.props.loadData()
    this.timer = this.props.startScheduler()
  }

  componentWillUnmount () {
    clearInterval(this.timer)
  }
  render () {
    return this.props.children
  }
}

const mapDispatchToProps = dispatch => {
  return {
    loadData: () => {dispatch(fetchCurrenciesAction())},
    startScheduler: () => {setInterval(()=>{dispatch(fetchCurrenciesAction())}, 60000)}
  }
}

export default connect(null, mapDispatchToProps)(Scheduler)
