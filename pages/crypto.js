import React from 'react'
import {connect} from 'react-redux'
import CryptoDetail from '../components/currency/CryptoDetail';

class Crypto extends React.Component {
  render () {
    return (
      <CryptoDetail />
    )
  }
}

export default connect()(Crypto)
