import { Component } from 'react'
import {connect} from 'react-redux'
import { Grid, Row, Col } from 'react-bootstrap'
import Price from './Price';
import CryptoRank from './CryptoRank';
import Header from './Header';
import Supply from './Supply';
import SiteHead from '../../SiteHead';
import { currencyDetailAction } from './fetchCurrencyDetail';
import { targetCurrency } from '../currencies/fetchCurrencies';

/**
 * Present trading detail for cryptocurrency
 * 
 * @param {Object} props
 * @param {Object} props.currency - the cryptocurrency containing trading detail to present
 */
class CryptoDetail extends Component {

  componentDidMount() {
    const currNameLoc = window.location.search.indexOf('currency=')
    this.currName = window.location.search.substring(currNameLoc+'currency='.length)
    this.props.loadCurrencyDetail(this.currName)
  }

  render() {
    const {currency, targetCurrency} = this.props
    return (    
      <div>
        <SiteHead title="Currency Detail" />
        <Grid fluid={true}>
          <Header currency={currency} />
          <div className={'currency_container'}>
            <style jsx>{`
                .currency_container {
                  background-color: #162a47;
                  color: white;
                  height: 1000px; 
                  margin: -15px;
                }

                .currency {
                  margin: 50px;
                  height: 160px;
                }

                .detail_container {
                  height: 100%;
                  line-height: 160px;
                  font-size: 10px;
                  font-weight: bold;
                  color: #5e737a; 
                }
              `}</style>
            <Row>
              <Col md={12}>
                <div className='currency'>
                  <div className='detail_container'>
                    <Grid>
                      {currency &&
                        <Row>
                          <Col md={4}>
                            <CryptoRank rank={currency.rank} />
                          </Col>
                          <Col md={4}>
                            <Price name={'MARKET CAP'} value={currency[`market_cap_${targetCurrency.toLowerCase()}`]} currency={targetCurrency}/>
                          </Col>
                          <Col md={4}>
                            <Price name={'24H VOLUME'} value={currency[`24h_volume_${targetCurrency.toLowerCase()}`]} currency={targetCurrency} />
                          </Col>
                          <Col md={4}>
                            <Supply name={'CIRCULATING SUPPLY'} value={currency.available_supply} symbol={currency.symbol}/>
                          </Col>
                          <Col md={4}>
                            <Supply name={'TOTAL SUPPLY'} value={currency.total_supply}  symbol={currency.symbol}/>
                          </Col>
                        </Row>
                      }
                    </Grid>
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </Grid>
      </div>
    )
  }
}

const mapStateToProps = ({currency, targetCurrency}) => {
  return { currency, targetCurrency }
}

const mapDispatchToProps = dispatch => {
  return {
    loadCurrencyDetail: currency => {dispatch(currencyDetailAction(currency))}
  }  
}

export default connect(mapStateToProps, mapDispatchToProps)(CryptoDetail)
