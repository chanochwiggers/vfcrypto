function Updated ({ lastUpdate }) {
  const updated = format(new Date(lastUpdate))
  return (
    <span>
      (Updated {updated})
    </span>
  )
}

export default Updated

const format = t => `${pad(t.getUTCHours())}:${pad(t.getUTCMinutes())}`

const pad = n => n < 10 ? `0${n}` : n
