export default function ChangeIndicator ({ percentChange }) {
  const pcDir = percentChange?(percentChange.startsWith('-')?'v':'^'):''
  return (
    <span className={pcDir==='^'?'change_up':'change_down'}>
      <style jsx>{`
        .change_up {
          color: #81c77e;
        }
        .change_down {
          color: #ff0065;
        }
      `}</style>
      {percentChange}% {pcDir}
    </span>
  )
}
